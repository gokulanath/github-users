import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class ProfileView extends React.Component {
  
  render() {
    const {
      name,
      company,
      followers,
      following,
      public_repos,
    } = this.props.user;
    return (
      <View style={{paddingBottom: 50}}>
        <View style={styles.infoContainer}>
          <Text style={styles.infoName}>
            {name}
          </Text>
          <Text style={styles.infoName}>
            {company}
          </Text>
        </View>
        <View style={{ flexDirection: 'row',justifyContent: 'space-around' }}>
          <View style={{ flexDirection: 'column' }}>
            <View style={styles.optionTextContainer}>
              <Text style={styles.infoText}>
                {followers}
              </Text>
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.infoText}>
                Followers &nbsp;
              </Text>
            </View>
          </View>

          <View style={{ flexDirection: 'column' }}>
            <View style={styles.optionTextContainer}>
              <Text style={styles.infoText}>
                {following}
              </Text>
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.infoText}>
                Following &nbsp;
              </Text>
            </View>
          </View>

          <View style={{ flexDirection: 'column' }}>
            <View style={styles.optionTextContainer}>
              <Text style={styles.infoText}>
                {public_repos}
              </Text>
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.infoText}>
                Repositories &nbsp;
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  optionTextContainer: {
    
  },
  infoContainer: {
    marginBottom: 15,
    marginTop: 15
  },
  infoText: {
    fontSize: 18,
    marginTop: 1,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff'
  },
  infoName: {
    fontSize: 18,
    marginTop: 3,
    marginBottom: 3,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
    
  }
});