import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { WebBrowser } from 'expo';

export default class Repositories extends React.Component {
  redirectToRepository({html_url}) {
    WebBrowser.openBrowserAsync(html_url);
  }
  render() {
    const {repositories} = this.props;
    console.log(repositories);
    return (
      <View>
        <View style={styles.repositoriesInfo}>
            <Text style={styles.title}>Repositories</Text>
          </View>
        {
          repositories.map((repository) => (
            <View key={repository.name}>
              <TouchableOpacity onPress={() => this.redirectToRepository(repository)}>
                <View style={styles.repository}>
                  <Text style={styles.repositoryName}>{repository.name}</Text>
                  <Text style={styles.repositoryStars}>{repository.stargazers_count} stars</Text>
                </View>
              </TouchableOpacity>
            </View>
          ))
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  repository: {
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderColor: '#eee',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },
  repositoriesInfo: {

  },
  title: {
    fontSize: 18,
    marginLeft: 10
  },
  repositoryName: {
    fontSize: 20,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    marginLeft: 10,
  },
  repositoryStars: {
    marginLeft: 10,
  },
});