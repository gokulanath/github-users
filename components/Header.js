import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = ({ title, marginLeft }) => (
  <View style={styles.header}>
    <Text style={headerText(marginLeft)}>{title}</Text>
  </View>
);
const styles = StyleSheet.create({
  header: {
    flex: 1,
    height: 50,
  },
});

const headerText = marginLeft => ({
  color: 'rgba(96,100,109, 1)',
  fontWeight: '700',
  fontSize: 20,
  marginLeft: marginLeft,
  marginTop: 12,
  textAlign: 'left',
});

export default Header;
