import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';
import {debounce} from 'lodash';

import { MonoText } from '../components/StyledText';
import { searchUsers } from '../utils/Api';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  state={
    search: '',
    loading: false,
    users: []
  }

  fetchUsers = debounce((text) => {
    console.log('debounced');
    this._fetchUsers(text)
  }, 300);

  async _fetchUsers(text) {
    console.log('debounced search', text);
    const { error, users } = await searchUsers(text);
      if(error && error !== 'Request Canceled' && Platform.OS !== 'ios') {
        ToastAndroid.showWithGravity(
          error,
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          loading: false
        });
      } else {
        if(users) {
          this.setState({
            users,
            loading: false
          });
        }
      }
    }

  onChangeSearch(text) {
    this.setState({
      search: text
    });
    if(text) {
      this.setState({
        loading: true
      });
      this.fetchUsers(text);
      //this.fetchUsers(text);
    }
  }

  handleClear() {
    this.setState({search: ''});
  }

  redirectToProfile({login, avatar_url}) {
    this.props.navigation.navigate('Profile', {
      login,
      avatar_url
    });
  }

  render() {
    const {loading, search, users} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Image
            source={
              require('../assets/images/octa-cat.png')
            }
            style={styles.welcomeImage}
          />
          <Text style={styles.headerText}>Search Github Users</Text>
        </View>

        <View style={styles.searchContainer}>
          <TextInput
            value={search}
            onChangeText={(text) => this.onChangeSearch(text)}
            style={styles.searchBox}
            placeholder="Search users"
          />
          <Text onPress={() => this.handleClear()} style={styles.clearSearchBox}>
            {search && 'Clear'}
          </Text>
        </View>

        {loading && <View style={styles.loadingContainer}>
          <Text style={styles.loading}>Loading...</Text>
        </View>}

        {users.length === 0 && <View style={styles.loadingContainer}>
          <Text style={styles.info}>Searched users appear here</Text>
        </View>}

        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          {
            users.map((user) => (
              <View key={user.login}>
                <TouchableOpacity onPress={() => this.redirectToProfile(user)}>
                  <View style={styles.user}>
                    <Image
                      source={
                        {'uri': user.avatar_url}
                      }
                      style={styles.userAvatar}
                    />
                    <Text style={styles.username}>{user.login}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            ))
          }
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 5,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 20,
  },
  headerText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    marginLeft: 10,
    textAlign: 'center',
  },
  searchContainer: {
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: '#eee',
    paddingTop: 10,
    paddingBottom: 10,
    position: 'relative'
  },
  searchBox: {
    borderWidth: 1,
    borderColor: '#bbb',
    paddingLeft: 10,
    paddingRight: 10,
    height: 40,
    borderRadius: 20
  },
  clearSearchBox: {
    position: 'absolute',
    right: 23,
    top: 20,
    fontSize: 14,
    color: '#2e78b7',
    textDecorationLine: 'underline'
  },
  loadingContainer: {
    justifyContent: 'center',
  },
  loading: {
    textAlign: 'center',
    fontSize: 18,
    color: '#bbb',
    fontWeight: 'bold'
  },
  info: {
    marginBottom: 50,
    textAlign: 'center',
    fontSize: 30,
    color: '#bbb',
    fontWeight: 'bold'
  },
  userList: {

  },
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#eee',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },
  userAvatar: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    borderRadius: 100
  },
  username: {
    fontSize: 20,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
    marginLeft: 10,
  },
  welcomeImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: -10,
  }
});
