import React from 'react';
import {Dimensions, Image, ScrollView, View, StyleSheet } from 'react-native';

import Header from './../components/Header';
import ProfileView from './../components/ProfileView';
import Repositories from './../components//Repositories';

import { fetchUser, fetchRepositories } from '../utils/Api';
export default class ProfileScreen extends React.Component {
  state={
    user: {},
    repositories: []
  }
  static navigationOptions = ({ navigation }) => {
    const title = navigation.getParam('login');
    return {
      //title: navigation.getParam('login'),
      headerTitle: <Header title={title}/>
    };
  };

  async fetchUser() {
    const { error, user } = await fetchUser(this.props.navigation.getParam('login'));
      if(error && error !== 'Request Canceled' && Platform.OS !== 'ios') {
        ToastAndroid.showWithGravity(
          error,
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
      } else {
        if(user.login) {
          this.setState({
            user
          });
        }
      }
  }

  async fetchRepositories() {
    const { error, repositories } = await fetchRepositories(this.props.navigation.getParam('login'));
      if(error && error !== 'Request Canceled' && Platform.OS !== 'ios') {
        ToastAndroid.showWithGravity(
          error,
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
      } else {
        if(repositories && repositories.length) {
          this.setState({
            repositories
          });
        }
      }
  }

  componentDidMount() {
    this.fetchUser();
    this.fetchRepositories();
  }

  render() {
    const avatarUrl = this.props.navigation.getParam('avatar_url');
    const login = this.props.navigation.getParam('login')
    return (
      <ScrollView style={styles.container}>
        <View style={styles.profileContainer}>
          <View style={styles.profileImageContainer()}>
            <Image
              source={
                {uri: avatarUrl}
              }
              style={styles.profileImage}
            />
          </View>
          <ProfileView 
            user={this.state.user}
          />
        </View>
        <Repositories
          login={login}
          repositories={this.state.repositories}
        />
        
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  profileContainer: {
    backgroundColor: '#333',
  },
  profileImageContainer: () => {
    const {height} = Dimensions.get('window')
    return {
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 50,
    }
  },
  profileImage: {
    borderRadius: 150,
    height: 100,
    width: 100,
  }
});
