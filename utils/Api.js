import axios, { CancelToken } from 'axios';
let source = null;

const searchUsers = (text) => new Promise(async (resolve) => {
  if(source){
    source.cancel('Operation cancelled');
  }
  source = CancelToken.source();
  try {
    const response = await axios.get(`https://api.github.com/search/users?q=${text}`, {
      cancelToken: source.token
    });
    console.log(response);
    resolve({
      error: null,
      users:response.data.items
    });
  } catch (error) {
    if (axios.isCancel(error)) {
      console.log('Request canceled', error);
      resolve({
        error: 'Request Canceled', 
        users: []
      });
    }else {
      resolve({
        error: 'Error in searching users', 
        users: []
      });
      console.log(error);
    }
  }
});

  const fetchUser = (login) => new Promise(async (resolve) => {
  try {
    const response = await axios.get(`https://api.github.com/users/${login}`);
    console.log(response);
    resolve({
      error: null,
      user: response.data
    });
  } catch (error) {
    if (axios.isCancel(error)) {
      console.log('Request canceled', error);
      resolve({
        error: 'Request Canceled', 
        user: {}
      });
    }else {
      resolve({
        error: 'Error in fetching user', 
        user: {}
      });
      console.log(error);
    }
  }
});

const fetchRepositories = (login) => new Promise(async (resolve) => {
  try {
    const response = await axios.get(`https://api.github.com/users/${login}/repos`);
    console.log(response);
    resolve({
      error: null,
      repositories: response.data 
    });
  } catch (error) {
    if (axios.isCancel(error)) {
      console.log('Request canceled', error);
      resolve({
        error: 'Request Canceled', 
        repositories: []
      });
    }else {
      resolve({
        error: 'Error in fetching Repositories', 
        repositories: []
      });
      console.log(error);
    }
  }
});

export {
  searchUsers,
  fetchUser,
  fetchRepositories
}